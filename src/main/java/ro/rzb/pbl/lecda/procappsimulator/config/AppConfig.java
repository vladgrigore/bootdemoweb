package ro.rzb.pbl.lecda.procappsimulator.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:foo.properties")
@Configuration
public class AppConfig {

    @Value("${test.prop}")
    private String testProp;

}
